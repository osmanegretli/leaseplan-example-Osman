package utils;

public class StringUtilities {
    public String firstLetterCapped(String inputString){ //Capitalizes the first letter of the input string

        if (inputString!=null){
            String firstLetter = inputString.substring(0, 1);
            String remainingLetters = inputString.substring(1);
            firstLetter = firstLetter.toUpperCase();

            return firstLetter + remainingLetters;
        }
        else
            return null;
    }
}
